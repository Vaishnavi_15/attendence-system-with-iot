# **Raspberry PI** 

- It is a series of small single board computers developed by the ***Raspberry Pi foundation***.
- It is a ***credit card sized computer*** that plugs into your TV / Monitor and a Keyboard.
<img src="https://www.watelectronics.com/wp-content/uploads/2019/07/Hardware-Specifications-of-Raspberry-pi.jpg" width="1000" height="800">

## RASPBERRY PI MODELS SPECIFICATIONS

<img src="https://core-electronics.com.au/media/wysiwyg/tutorials/sam/raspberry-pi-boards-connectivity-table.png" width="1000" height="700">


## HARDWARE COMPONENTS

<img src="https://www.researchgate.net/profile/Zakaria_Rada2/publication/312218161/figure/fig8/AS:449572641218569@1484197878973/Raspberry-Pi-Setup-with-all-components.png" width="900" height="600">

 - **USB ports** — These are used to connect a mouse and keyboard. 
 - **SD card slot** — It is used to insert SD Card into Raspberry Pi. SD Card contains all the files and Raspberry Pi OS necessary to use Raspberry Pi.
 - **Ethernet port** — It is used to connect Raspberry Pi to a network with a cable. Raspberry Pi can also connect to a network via wireless LAN.
 - **Audio jack** — We can connect headphones or speakers here.
 - **HDMI port** — This is the place, where we connect the monitor (or projector) that we are using to display the output from the Raspberry Pi. If the monitor has speakers, we can also use them to hear sound.
 - **Micro USB power connector** — It is where we connect a power supply. It should always be done at last.
 - **GPIO pins** — It stands for General Purpose Input Output Pins. These allow us to connect electronic components such as LEDs and buttons to Raspberry Pi.

 - **Camera Module Jack** - It is used to connect camera to Raspberry Pi for recording video.


## OPERATING SYSTEM REQUIRED TO INSTALL RASPBERRY PI: RASPBIAN

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTurT1e3C8mg5UN2cjbUE876AaHA_q3lfPn6Q&usqp=CAU" width="600" height="300">

 Using [Raspbian Image file](https://www.raspberrypi.org/downloads/) :  A **Raspbian image** is a file that you can download onto an SD card which in turn can be used to boot your Raspberry Pi and Via APC into the Raspbian operating system.


