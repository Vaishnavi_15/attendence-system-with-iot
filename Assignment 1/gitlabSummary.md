## About GIT

- Git is a ***version-control software***.
- Git lets you easily *keep track* of every revision you and your team make during the development of your software. 
- You also do not need to be connected all the time because the project is both saved locally on each machine and remotely (probably at Github).

## Required Vocabulary
- **Repository**: It is the *collection of files and folders* that you’re using git to track. 
- **Commit**: It is used to *save your changes to the local repository*.
- **Push**: Pushing is essentially *syncing your commits to the cloud* (again, probably Github). 
- **Branch**: These are separate instances of the code that offshoots from the main codebase. 
- **Merge**: When a branch is polished up, free of bugs, and ready to become part of the primary codebase, it will get merged into the master branch.
- **Clone**:  It takes the entire online repository and makes an ***exact copy of it on your local machine***.
- **Fork**: It means you just create a copy of the main repository of a project source code to your own GitHub profile.
- **Pull Request**: It is when you *submit a request for the changes* you have made to be pulled into the Master Branch of the repository. 

## About GitLab

<img src="https://image.slidesharecdn.com/webinarcontinuousintegrationwithgitlab-160405075624/95/webinar-continuous-integration-with-gitlab-6-638.jpg?cb=1459843017" width="700" height="500">

## GitLab Workflow


#### 1. Clone project
    git clone git@example.com:project-name.git
#### 2. Create branch with your feature
    git checkout -b $feature_name
#### 3. Write code. Commit changes
    git commit -am "My feature is ready"
#### 4. Push your branch to GitLab
    git push origin $feature_name
#### 5. Review your code on commits page
#### 6. Create a merge request
#### 7. Your team lead will review the code & merge it to the main branch

