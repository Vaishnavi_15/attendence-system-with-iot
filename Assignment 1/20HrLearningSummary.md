> # **EXPAND  YOUR  PERSPECTIVES**

## MOST COMMON EXCUSE: I am ***never*** going to have free time ***ever again***.

#### How long does it take to acquire a new skill?

According to references, it takes **10,000 hours** to master a skill, which translates to about 9 years (consider 5 days a week, spending 4 hours a day). **But**, in actual, it takes **20 Hours** of focused deliberate practice.

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR9uxTgnvooffuJ_omJgUwGzGbwGTu6AOxSrA&usqp=CAU" width="800" height="500">

### **How to learn a new skill ?**
- Deconstruct the skill

<img src="https://slideplayer.com/slide/16750971/97/images/13/Deconstruct+the+skill.jpg" width="900" height="500">

- Learn enough to self correct

<img src="https://image.slidesharecdn.com/pankajkumar30262tedtalkpersonalbranding-160221175927/95/mbaskillsin-how-to-learn-anything-17-638.jpg?cb=1457349421" width="900" height="500">

- Remove practice barriers

<img src="https://www.fmcsa.dot.gov/sites/fmcsa.dot.gov/files/docs/StayFocusedB.jpg" width="900" height="500">

- Practice at-least for 20 Hours

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVVWguK0ONVRlonQK08t3QFwFVifQzbjY09g&usqp=CAU" width="900" height="500">

> ## The major barrier to skill acquistion is not intellectual, it is ***emotional***.

### LEARNING CURVE

<img src="https://fixwillpower.com/wp-content/uploads/the-first-20-hours-learning-curve-720x405.png" width="800" height="500">

This graph indicates that *how good you are* vs *practice time*. It indicates that in starting , we need to work harder to learn a skill, after that, the success rate is high.
